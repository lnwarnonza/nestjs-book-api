import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type BookDocument = Book & Document;

@Schema()
export class Book {

  @Prop()
  title?: string;
    
  @Prop()
  category?: string;
    
  @Prop({default: 10})
  amount!: Number;

  @Prop()
  price?: Number;

  @Prop({default: 0})
  soldAmount?: string;

  @Prop({ default: Date.now })
  createdAt!: Date;

  @Prop({ default: Date.now })
  updatedAt!: Date;
 
}


export const BookSchema = SchemaFactory.createForClass(Book);