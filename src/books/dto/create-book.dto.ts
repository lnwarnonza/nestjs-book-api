export class CreateBookDto {
    title: string;
    category: string;
    amount: string;
    price: string;
}
