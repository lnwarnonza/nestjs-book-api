import { Injectable, ConflictException, NotFoundException, Req } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Request } from "express";
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { Book, BookDocument ,} from './schemas/book.schema';

@Injectable()
export class BooksService {
  constructor(@InjectModel(Book.name) private bookModel: Model<BookDocument>) { }

  async createBook(createBookDto: CreateBookDto): Promise<Book> {
    try {
      const createBook = new this.bookModel(createBookDto)
      return await createBook.save();
  } catch(e) {
      throw new ConflictException({
          message: ['Something\s wrong I can feel it.']
      })
  }
  }

  async findBookAll(): Promise<Book[]> {
    try {
      return this.bookModel.find().exec();
    } catch(e) {
      throw new NotFoundException({
          message: ['book not found.']
      })
  }
  }


  async findMany(@Req() req: Request) {
    let options = {};
    let sort = {};
    if (req.query.sort) {
      sort = { amount: req.query.sort}
    }
    if (req.query.search) {
      options = {
        $or: [
          { title: new RegExp(req.query.search.toString(),) },
          {category: new RegExp(req.query.search.toString())}
        ]         
      }
    }
    return this.bookModel.find(options).sort( sort ).exec();
  }

  // async checkAmountBook(book) {
  //  if( book.amount < )
  // }


  async findOrderBookById(_id) {
    try {
    const book = await this.bookModel.findOne({ _id })
      return book
    } catch(e) {
      throw new NotFoundException({
          message: ['book not found.']
      })
  }
  }

  async findBookById(_id: string) {
    try {
    const book = await this.bookModel.findOne({ _id })
      return book
    } catch(e) {
      throw new NotFoundException({
          message: ['book not found.']
      })
  }
  }

  async updateBook(_id: string, updateBookDto: UpdateBookDto) {
    try {
      const book = await this.findBookById(_id);
      await this.bookModel.updateOne({ _id }, { $set: { ...updateBookDto } })
      return book
    } catch (e) {
      throw new NotFoundException({
        message: ['Book is not update']
      })
    }
  }

  async deleteBook(_id: string) {
    try {
    const book = await this.findBookById(_id);
    await this.bookModel.deleteOne({ _id })
      return book
    } catch(e) {
      throw new NotFoundException({
          message: ['Never gonna give you up.']
      })
  }
  }
}
