import { Controller, Get, Post, Body, Patch, Param, Delete, Req, } from '@nestjs/common';
import { Request } from "express";
import { BooksService } from './books.service';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';



@Controller('books')
export class BooksController {
  constructor(private readonly booksService: BooksService) { }

  @Post()
  createBook(@Body() createBookDto: CreateBookDto) {
    return this.booksService.createBook(createBookDto);
  }

  @Get()
  findBookAll() {
    return this.booksService.findBookAll();
  }
  @Get('filter')
  findMany(@Req() req: Request){
    return this.booksService.findMany(req);

  }

  @Get(':_id')
  findOne(@Param('_id') _id: string) {
    return this.booksService.findBookById(_id);
  }

  @Patch(':_id')
  updateBook(@Param('_id') _id: string, @Body() updateBookDto: UpdateBookDto) {
    return this.booksService.updateBook(_id, updateBookDto);
  }

  @Delete(':_id')
  deleteBook(@Param('_id') _id: string) {
    return this.booksService.deleteBook(_id);
  }
}
