import { InjectModel } from '@nestjs/mongoose';
import { Injectable, NotFoundException ,ConflictException,HttpException, HttpStatus,Req} from '@nestjs/common';
import { Model } from 'mongoose';
import { Request } from "express";
import * as bcrypt from 'bcrypt';
import { addSeconds } from 'date-fns';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument  } from './schemas/user.schema';
import { UserDetails } from './interfaces/user-details.interface';


@Injectable()
export class UsersService {
  SECOND_TO_VERIFY = 30;
  STATUS_TO_BLOCK = 0;
  STATUS_NOT_BLOCK = 1;
  LOGIN_ATTEMPTS_TO_BLOCK = 3;  
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>,) { }

  _getUserDetails(user: Readonly<UserDocument>): UserDetails{
    return {
      id: user._id,
      firtname: user.firtname,
      lastname: user.lastname,
      username: user.username,
      wallet: user.wallet,
      blockStatus: user.blockStatus,
    };
  }

  async createUser(
    firtname: string,
    lastname: string,
    username: string,
    hashedPassword: string,): Promise<UserDocument>{
    const newUser = new this.userModel({
      firtname,
      lastname,
      username,
      password: hashedPassword
    });
    return newUser.save();
  }
  
  
  findOneUsername(username: string): Promise<UserDocument  | undefined> {
    return this.userModel.findOne({ username }).exec();
  }
  
  async findUserByUsername(username: string): Promise<UserDetails> {
    return this.userModel.findOne({username, verified: true});
  }

  async checkPassword(attemptPass: string, user) {
      const match = await bcrypt.compare(attemptPass, user.password);
      if (!match) {
        await this.passwordDoNotMatch(user);
        throw new NotFoundException({ message: 'Wrong  password, Please try again.' })
      }
  }

  async passwordDoNotMatch(user) {
    user.loginAttempts += 1;
    await user.save();
    if (user.loginAttempts > this.LOGIN_ATTEMPTS_TO_BLOCK) {
      await this.blockUserTimeAttempts(user);
      throw new ConflictException({ message: 'User has been blocked,Please waitng 30 second.' })
    }
  }

  async passwordAreMatch(user) {
    user.loginAttempts = 0;
    await user.save();
  }

    isUserBlocked(user) {
    if (user.blockStatus < this.STATUS_NOT_BLOCK) {
      throw new ConflictException({ message: 'User has been blocked try later.'});
    }
    }
     
    isUserBlockedTimeAttempts(user) {
      if (user.timeAttemps > Date.now()) {
          throw new ConflictException({ message: 'User has been blocked,Please waitng 30 second.' });
        }
      } 
  
 async blockUserTimeAttempts(user) {
    user.timeAttemps = addSeconds(new Date(), this.SECOND_TO_VERIFY)
  await user.save();
 }
  
 async blockedUser(user) {
  user.blockStatus = this.STATUS_TO_BLOCK
   await user.save();
   
}
  
  async blockUser(_id: string): Promise<UserDetails> {
    try {
      const user = await this.userModel.findById(_id).exec();
      await this.blockedUser(user)
    return this._getUserDetails(user)
    } catch (e) {
      throw new NotFoundException({
        message: ['user is not block']
      })
    }
}
  
  async findUserMany(@Req() req: Request): Promise<any> {
    let options = {};
    if (req.query.search) {
      options = {
        $or: [
          { firtname: new RegExp(req.query.search.toString())},
          { lastname: new RegExp(req.query.search.toString()) },
          { username: new RegExp(req.query.search.toString())}
        ]         
      }
    }
    return this.userModel.find(options).exec();
  }


  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      return new this.userModel(createUserDto).save();
    } catch (e) {
      throw new NotFoundException({
        message: ['can not create user']
      })
    }
  }


  async findAll(): Promise<User[]> {
    try {
      const users = await this.userModel.find();
      return users
    } catch(e) {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        message: 'User not found.',
      }, HttpStatus.FORBIDDEN);
    }
  }

  async findUserById(id: string):Promise<UserDetails>  {
    try {
      const user = await this.userModel.findById(id).exec();
      return this._getUserDetails(user);
    } catch(e) {
      throw new NotFoundException({
        status: HttpStatus.FORBIDDEN,
        message: 'User not found.',
      })
    }
  }


  async updateUser(_id: string, updateUserDto: UpdateUserDto){
    try {
      const user = await this.findUserById(_id);
      const { firtname, lastname, username } = updateUserDto
      if (firtname) {
        user.firtname = firtname
      }
      if (lastname) {
        user.lastname = lastname
      }
      if (username) {
        user.username = username
      }
      await this.userModel.updateOne({ _id }, { $set: { ...updateUserDto } })
      return user
    } catch (e) {
      throw new NotFoundException({
        message: ['user is not update']
      })
    }
  }

  async deleteUser(_id: string) {
    try {
      const user = await this.findUserById(_id);
      await this.userModel.deleteOne({_id})
      return user
    } catch (e) {
      throw new NotFoundException({
        message: [' user is not delete']
      })
    }
  }

}
