export interface UserDetails {
    id: string;
    firtname: string;
    lastname: string;
    username: string;
    wallet: string;
    blockStatus: string;
}