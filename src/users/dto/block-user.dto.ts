import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

export class BlockUserDto extends PartialType(CreateUserDto) {
    blockStatus?: string;
  
}
