import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Matches } from 'class-validator';
export class CreateUserDto {

 
    @Prop()
    firtname: string;
    
   @Prop()
    lastname: string;
    
    @Prop({ required: true, unique: true })
    username: string;

    @Prop({ required: true })
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message: 'Password must be Minimum eight characters, at least one letter and one number.'
      })
  password: string;
  // @Prop() createdAt?: Date
   
}
