import { Prop} from '@nestjs/mongoose';
import { IsNotEmpty } from 'class-validator';
export class LoginUserDto {

  @IsNotEmpty()
  @Prop({ required: true,  })
  readonly username: string;
  
    @IsNotEmpty()
    @Prop({ required: true })
    readonly  password: string;
  // @Prop() createdAt?: Date
   
}
