import { Controller, Get, Post, Body, Patch, Param, Delete,Req} from '@nestjs/common';
import { UsersService } from './users.service';
import { Request } from "express";
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDetails } from './interfaces/user-details.interface';
import { BlockUserDto } from './dto/block-user.dto';
@Controller('users')
export class UsersController {

  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
      return this.usersService.create(createUserDto);
  }
  
  @Get()
  findAll() {
    return this.usersService.findAll();
  }


   @Get(':_id')
   findUserById
    (@Param('_id') _id: string,){
      return this.usersService.findUserById(_id);
    }
    

  @Patch(':_id')
  updateUser(@Param('_id') _id: string, @Body() updateUserDto: UpdateUserDto): Promise<UserDetails> {
    return this.usersService.updateUser(_id, updateUserDto);
  }

  @Delete(':_id')
  remove(@Param('_id') _id: string): Promise<UserDetails> {
    return this.usersService.deleteUser(_id);
  }

  @Post('filter')
  findUserMany(@Req() req: Request){
    return this.usersService.findUserMany(req);

  }

  @Patch('blockuser/:_id')
  blockUser(@Param('_id') _id: string): Promise<UserDetails> {
    return this.usersService.blockUser(_id);
  }

}
