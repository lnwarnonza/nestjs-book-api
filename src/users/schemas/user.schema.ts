import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type UserDocument = User & Document;

@Schema()
export class User {


  @Prop()
    firtname?: string;
    
  @Prop()
    lastname?: string;
    
  @Prop()
  username?: string;

  @Prop()
  password?: string;

  @Prop({ default: 0 })
  loginAttempts!: Number;

  @Prop({ defual: Date.now })
  timeAttemps!: Date;

  @Prop({default: 1})
  blockStatus!: string;

  @Prop()
  wallet?: string;

  @Prop({ default: Date.now })
  createdAt!: Date;

  // @Prop( type => Order, order => order.user )
  // order: Order
 
}


export const UserSchema = SchemaFactory.createForClass(User);