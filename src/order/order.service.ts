import { Injectable,UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order, OrderDocument } from './schemas/order.schema';
import { User } from 'src/users/schemas/user.schema';
import { BooksService } from 'src/books/books.service';
import { Book } from 'src/books/schemas/book.schema';
@Injectable()

export class OrderService {
  constructor(
    private booksService:BooksService,
    @InjectModel(Order.name) private orderModel: Model<OrderDocument>) { }

  async create(createOrderDto: CreateOrderDto, user: User): Promise<Order> {
    const { book } = createOrderDto
    //  await this.findBookById(_id)
    const order = await this.createOrder(book,user)  
    return order
  }

 

  async createOrder(book: string, user: User): Promise<OrderDocument>{
    const order = new this.orderModel({
      book,
      user
    });
    return order.save();
  }

  async findBookById(_id: string): Promise<Book | null>{
    const books = await this.booksService.findOrderBookById(_id);
    if (!books) {
        throw new UnauthorizedException({message: 'books not found.'});
    }
    return books;
}

  async recordAllOrder():Promise<Order[]>  {
    return await this.orderModel.find()
  }

  async recordOnlyOrder(user: User):Promise<Order[]>  {
    return await this.orderModel.find({user})
  }

}
