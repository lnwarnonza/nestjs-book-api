import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { Order, OrderSchema } from './schemas/order.schema';
import { BooksService } from 'src/books/books.service';
import { Book, BookSchema } from 'src/books/schemas/book.schema';
@Module({
  imports:[MongooseModule.forFeature([{ name: Order.name,schema: OrderSchema}]),MongooseModule.forFeature([{ name: Book.name, schema: BookSchema }]),],
  controllers: [OrderController],
  providers: [OrderService,BooksService]
})
export class OrderModule {}
