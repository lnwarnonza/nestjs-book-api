import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from 'src/users/schemas/user.schema';
import { Book } from 'src/books/schemas/book.schema';
export type OrderDocument = Order & Document;

@Schema()
export class Order {


  @Prop({type: mongoose.Schema.Types.Array, ref: 'User'} )
  user: User;
    
  @Prop({type: mongoose.Schema.Types.Array, ref: 'Book'})
  book: Book;
    
  @Prop({ default: Date.now })
  createdAt!: Date;
 
}


export const OrderSchema = SchemaFactory.createForClass(Order);