import { Controller, Get, Post, Body, UseGuards, Request } from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';
import { Order } from './schemas/order.schema';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @UseGuards(JwtAuthGuard)
  @Post('buy')
  create(@Body() createOrderDto: CreateOrderDto,
          @Request() { user }: any
  ):Promise<Order> {
    return this.orderService.create(createOrderDto,user);
  }

  @Get('recordall')
  recordAllOrder() {
    return this.orderService.recordAllOrder();
  }

  @UseGuards(JwtAuthGuard)
  @Get('recordonly')
  recordOnlyrder(@Request() { user }: any) {
    return this.orderService.recordOnlyOrder(user);
  }

 
}
