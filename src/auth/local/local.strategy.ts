import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { JwtPayload } from '../interfaces/jwt-payload.interface';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(payload: any) {
    // const user = await this.authService.validateUser(jwtPayload);
    // if (!user) {
    //     throw new UnauthorizedException({
    //       message: ['Wrong username or password.']
    //   });
    // }
    return { ...payload.user};
  }
  // async validate(username: string, password: string): Promise<any> {
  //   const user = await this.authService.validateUser(username, password);
  //   if (!user) {
  //       throw new UnauthorizedException({
  //         message: ['Wrong username or password.']
  //     });
  //   }
  //   return user;
  // }
}