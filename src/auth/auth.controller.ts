import { Controller, Get, Post, Body, UseGuards, Req } from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserDetails } from 'src/users/interfaces/user-details.interface';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local/local-auth.guard';
import { JwtAuthGuard } from './jwt/jwt-auth.guard';


@Controller('auth')
export class AuthController {
  
  constructor(
    private authService: AuthService) { }

    @Post('signup')
    register(@Body() createUserDto: CreateUserDto): Promise<UserDetails> {
        return this.authService.signUp(createUserDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Body() loginUserDto:LoginUserDto): Promise<any>{
    return this.authService.logIn(loginUserDto)
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Req() req: any): Promise<any>{
    return  req.user
  }

  }