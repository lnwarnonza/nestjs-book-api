import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/users/schemas/user.schema';
import { UsersService } from 'src/users/users.service';
import { UsersModule } from 'src/users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule,ConfigService } from '@nestjs/config';
import { JwtStrategy } from './jwt/jwt.strategy';
@Module({
  imports: [PassportModule,MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    // , UsersModule
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => {
        return{
          secret: config.get<string>('JWT_SECRET'),
          signOptions: { expiresIn: '2 days'},
        }
      }
    })
    ],
    providers: [AuthService,UsersService ,LocalStrategy,JwtStrategy,ConfigService],
  controllers: [AuthController],

  
 
})
export class AuthModule {}
