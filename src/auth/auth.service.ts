import { Injectable, HttpException, HttpStatus,UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserDetails } from 'src/users/interfaces/user-details.interface';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
import * as bcrypt from 'bcrypt';


@Injectable()
export class AuthService {
    
    constructor(   
        private userService: UsersService,
        private jwtService: JwtService,
    ) { }

    async validateUser(username: string): Promise<UserDetails | null>{
        const user = await this.userService.findUserByUsername(username);
        if (!user) {
            throw new UnauthorizedException({message: 'User not found.'});
        }
        return user;
    }

    async logIn(loginUserDto: LoginUserDto) {
        const { username } = loginUserDto;
        const user = await this.validateUser(username);
        const doesUserExist = !!user; 
        if (!doesUserExist) return null; 
        this.userService.isUserBlocked(user)
        this.userService.isUserBlockedTimeAttempts(user)
        await this.userService.checkPassword(loginUserDto.password, user);
        await this.userService.passwordAreMatch(user)
        const payload = { username: user.username , sub: user.id}
        const token =  this.jwtService.sign(payload)
        return {
            token
        }
    }
    
    async signUp(createUserDto: Readonly<CreateUserDto>): Promise<UserDetails | null>{
            const {
              firtname,
              lastname,
              username,
              password,
            } = createUserDto
            const existingUser = await this.userService.findOneUsername(username)
            if (existingUser) {
                throw new HttpException('username already exists', HttpStatus.BAD_REQUEST);
            }
            const hashedPassword = await bcrypt.hash(password,10)
            const newUser = await this.userService.createUser(
                firtname,
                lastname,
                username,
                hashedPassword,        
            )          
            return this.userService._getUserDetails(newUser);
   
    }
}
